﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Tasks = System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Exchange.WebServices.Data;
using System.Security;
using System.Diagnostics;
using System.Security.Principal;
using DnsRip;
using System.Net.NetworkInformation;
using System.Net.Sockets;

namespace mail_flow_monitor
{
    public static class Program
    {
        private static int Result { get; set; }
        private static DateTime LastSendDate { get; set; }
        private static string LastSendMessageId { get; set; }
        private static ExchangeService exchService;
        public static EventLog EventLog { get; set; }
        private static List<string> SMTPServers { get; set; }
        private static List<string> host;
        private static int port;
        private static string from;
        private static string to;
        private static string subject;
        private static string body;
        private static string exchangeServer;
        private static int delay;
        private static bool portLogging;
        private static bool eventLogging;
        private static bool useNotification;
        private static bool deliveryResult=false;
        const int notifLifetime = 30;

        private enum ErrorCodes
        {
            ArgumentError = 1001,
            ParseArgumentError = 1002,
            MXLookupError = 2001,
            DNSError = 2002,
            ConnectError = 2003,
            CredentialsError = 2004,
            ProxyError = 2005,
            SMTPSendError = 2006,
            SMTPCheckError = 2007,
            OtherError = 2000
        }
        private enum SuccessCodes
        {
            CredentialsOK = 3001,
            MXLookupOK = 3002,
            EmailSendOK = 3003,
            EmailCheckOK = 3004
        };
        public static void Main(string[] args)
        {
            EventLog = new EventLog();
            var comlApp = new CommandLineApplication();
            comlApp.HelpOption("-h|--help");
            CommandOption hostOpt = comlApp.Option("-H|--host", "Smtp server(s) to receive incoming email. If specified, overrides MX lookup", CommandOptionType.MultipleValue);
            CommandOption portOpt = comlApp.Option("-p|--port", "Smtp port to connect (default smtp port 25).", CommandOptionType.SingleValue);
            CommandOption fromOpt = comlApp.Option("-f|--from", "Set 'From:' email field", CommandOptionType.SingleValue);
            CommandOption toOpt = comlApp.Option("-t|--to", "Set 'To:' email field", CommandOptionType.SingleValue);
            CommandOption subjOpt = comlApp.Option("-s|--subject", "Set 'Subject:' email field", CommandOptionType.SingleValue);
            CommandOption bodyOpt = comlApp.Option("-b|--body", "Set email body text", CommandOptionType.SingleValue);
            CommandOption exchOpt = comlApp.Option("-x|--exchange", "Exchange server EWS address to check the mail delivery", CommandOptionType.SingleValue);
            CommandOption noteOpt = comlApp.Option("-n|--notification", "Use notification subscription to check the delivery result (lifetime=30mins.)", CommandOptionType.NoValue);
            CommandOption delayOpt = comlApp.Option("-d|--delay", "Delay the time of the email check (1000 = 1 sec.)", CommandOptionType.SingleValue);
            CommandOption logOpt = comlApp.Option("-l|--log", "Output the smtp protocol trace log", CommandOptionType.NoValue);
            CommandOption eventOpt = comlApp.Option("-e|--eventlog", "Log events to the Application eventlog. The first run requires admin privileges for an event source registration", CommandOptionType.NoValue);

            comlApp.OnExecute(() =>
            {
                try
                {
                    if (hostOpt.HasValue()) host = hostOpt.Values;
                    if (portOpt.HasValue()) port = int.Parse(portOpt.Value()); else port = 25;
                    if (fromOpt.HasValue()) from = fromOpt.Value(); else throw new ArgumentException("The required option is missing", "--from");
                    if (toOpt.HasValue()) to = toOpt.Value(); else throw new ArgumentException("The required option is missing", "--to");
                    if (subjOpt.HasValue()) subject = subjOpt.Value(); else subject = "Test Message";
                    if (bodyOpt.HasValue()) body = bodyOpt.Value(); else body = "Test Message";
                    if (exchOpt.HasValue()) exchangeServer = exchOpt.Value(); else throw new ArgumentException("The required option is missing", "--exchange");
                    if (delayOpt.HasValue()) delay = int.Parse(delayOpt.Value()); else delay = 60000;

                    useNotification = noteOpt.HasValue();
                    portLogging = logOpt.HasValue();
                    eventLogging = eventOpt.HasValue();


                }
                catch (ArgumentException e)
                {
                    Console.WriteLine($"Arguments parsing exception, use -h for help. {e.Message}");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Unknown argument parsing exception: {e.Message}");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
                return 0;
            });

            try
            {
                comlApp.Execute(args);
                if (comlApp.IsShowingInformation)
                {
                    Console.WriteLine("Hit any key to exit");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"{e.Message}");
            }

            string domain, user;
            SecureString password;

            bool isElevated;
            using (var identity = WindowsIdentity.GetCurrent())
            {
                var principal = new WindowsPrincipal(identity);
                isElevated = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }

            const string ApplicationName = "MailFlowMon";
            if (isElevated)
            {
                // Если не существует источника с именем нашего приложения, создаем его // Создаем источник в разделе Application
                if (!EventLog.SourceExists(ApplicationName)) EventLog.CreateEventSource(ApplicationName, "Application");
                EventLog.Source = ApplicationName;
            }
            else
            {
                EventLog.Log = "Application";
                EventLog.Source = ApplicationName;
            }

            exchService = new ExchangeService(ExchangeVersion.Exchange2013_SP1);
            do
            {
                GetCredentials(out user, out password, out domain);
            } while (CheckCredentials(exchangeServer, domain, password, user));
            password.MakeReadOnly();

            if (Result==0)
            {
                Console.WriteLine("Credentials are OK, sending the email");
                WriteLog("Credentials are OK, sending the email", EventLogEntryType.Information, (int)SuccessCodes.CredentialsOK, 0);

                if (useNotification) SubscribeToStreamNotification();
            }

            AppDomain.CurrentDomain.ProcessExit += (s,e) =>
            {
                password.Dispose();
                EventLog.Close();
            };

            while (true)
            {
                if (Result == 0)
                {
                    if (!hostOpt.HasValue())
                    {
                        var mailDomain = to.Split('@')[1];
                        try
                        {
                            SMTPServers = GetMXRecords(mailDomain);
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine($"DNS lookup error: " + e.Message);
                            Console.WriteLine($"-H (--host) option can be used instead.");
                            WriteLog($"MX records resolving error for domain {mailDomain}: " + e.Message, EventLogEntryType.Error, (int)ErrorCodes.MXLookupError, 0);
                            Result = e.GetHashCode();
                        }
                        if (SMTPServers.Count > 0)
                        {
                            Console.WriteLine($"MX record(s) found for domain {mailDomain} successfuly.");
                            WriteLog($"MX record(s) found for domain {mailDomain} successfuly.", EventLogEntryType.Information, (int)SuccessCodes.MXLookupOK, 0);
                        }
                        else
                        {
                            Console.WriteLine($"MX records for domain {mailDomain} could not be found!");
                            WriteLog($"MX records resolving error for domain {mailDomain}.", EventLogEntryType.Error, (int)ErrorCodes.MXLookupError, 0);
                            Result = (int)ErrorCodes.MXLookupError;
                        }
                    }
                    else
                    {
                        SMTPServers = host;
                    }
                }

                if (Result == 0)
                {
                    var task1 = SendEmailAsync(SMTPServers, port, from, to, subject, body, portLogging);
                    task1.Wait();
                }

                if (Result == 0)
                {
                    Console.WriteLine("Email has been sent successfuly, waiting for timeout:" + delay.ToString());
                    WriteLog("Email has been sent successfuly, waiting for timeout:" + delay.ToString(), EventLogEntryType.Information, (int)SuccessCodes.EmailSendOK, 0);
                    if (!useNotification)
                    {
                        var task2 = Tasks.Task.Delay(delay);
                        task2.Wait();
                        task2 = CheckMailAsync();
                        task2.Wait(); 
                    }
                    else
                    {
                        var task2 = Tasks.Task.Delay(delay);
                        task2.Wait();
                        if (deliveryResult)
                        {
                            Console.WriteLine("Message found successfuly!");
                            WriteLog("Message found successfuly! Message-ID: " + LastSendMessageId, EventLogEntryType.Information, (int)SuccessCodes.EmailCheckOK, 0);
                        }
                        else
                        {
                            Console.WriteLine("Message was not found or timed out!");
                            WriteLog("Message was not found or timed out! Message-ID: " + LastSendMessageId, EventLogEntryType.Error, (int)ErrorCodes.SMTPCheckError, 0);
                        }
                        deliveryResult = false;
                        LastSendMessageId = string.Empty;
                    }
                    var task3 = Tasks.Task.Delay(1000);
                    task3.Wait();
                    Console.Clear();
                }
                else
                {
                    Result = 0;
                    var task = Tasks.Task.Delay(delay);
                    task.Wait();
                    Console.Clear();
                }
            }


        }

        private static List<string> GetMXRecords(string mailDomain)
        {
            IEnumerable<string> DNSs = GetLocalDnsAddresses().Select(ip => new DnsRip.Parser().Parse(ip.ToString()).Parsed);
                return new DnsRip.Resolver(DNSs)
                         .Resolve(mailDomain, QueryType.MX)
                         .Select(r => r.Record)
                         .Distinct()
                         .Select(r =>
                         {
                             var recValues = r.Split(' ');
                             return new KeyValuePair<string, int>(recValues[1], int.Parse(recValues[0]));
                         })
                         .OrderBy(kvp => kvp.Value)
                         .Select(kvp => kvp.Key)
                         .ToList();
        }

        private static void WriteLog(string _message, EventLogEntryType _type, int _eventId, short _category)
        {
            if (eventLogging) EventLog.WriteEntry(_message, _type, _eventId, _category);
        }

        private static bool CheckCredentials(string server, string domain, SecureString password, string user)
        {
            bool retry = false;
            Result = 0;
            exchService.Credentials = new NetworkCredential(user, password, domain);
            exchService.Url = new Uri("https://" + server + "/EWS/Exchange.asmx");
            Console.WriteLine("Connecting and checking credentials...");

            try
            {
                var task = Tasks.Task.Run(() => Folder.Bind(exchService, WellKnownFolderName.Inbox));
                task.Wait();
            }
            catch (AggregateException aggregatedEx)
            {
                var e = aggregatedEx.InnerException.InnerException as WebException;
                Result = e.GetHashCode();
                var resp = e?.Response as HttpWebResponse;
                if (e?.Status == WebExceptionStatus.NameResolutionFailure)
                {
                    Console.WriteLine("Cannot resolve the server name! " + e.Message);
                    WriteLog("Cannot resolve the server name! Check the server name or DNS settings." + Environment.NewLine + e.Message, EventLogEntryType.Error, (int)ErrorCodes.DNSError, 0);
                }
                else
                if (e?.Status == WebExceptionStatus.ConnectFailure)
                {
                    Console.WriteLine("Cannot connect to the server! " + e.InnerException.Message);
                    WriteLog("Cannot connect to the server! Check the server address/name and its availability" + Environment.NewLine + e.InnerException.Message, EventLogEntryType.Error, (int)ErrorCodes.ConnectError, 0);
                }
                else
                if (resp?.StatusCode == HttpStatusCode.Unauthorized)
                {
                    Console.WriteLine("Wrong credentials! Retry? (Y/N)");
                    WriteLog("Wrong Exchange credentials", EventLogEntryType.Warning, (int)ErrorCodes.CredentialsError, 0);
                    string answer = Console.ReadLine();
                    if (answer.ToUpperInvariant() == "Y" || answer.ToUpperInvariant() == "YES")
                        retry = true;
                }
                else
                if (resp?.StatusCode == HttpStatusCode.BadGateway)
                {
                    Console.WriteLine("Cannot connect to the server. Proxy error: Bad proxy gateway! " + resp.ResponseUri);
                    WriteLog("Cannot connect to the server. Proxy error: Bad proxy gateway!"+ resp.ResponseUri, EventLogEntryType.Error, (int)ErrorCodes.ProxyError, 0);
                }
                else
                {
                    Console.WriteLine(e.Message);
                    WriteLog("Unknown connection error!" + Environment.NewLine + e.Message, EventLogEntryType.Error, (int)ErrorCodes.OtherError, 0);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return retry;
        }

        private static void GetCredentials(out string user, out SecureString password, out string domain)
        {
            Console.Write("username (mailbox): ");
            user = Console.ReadLine();
            Console.Write("password: ");
            password = ReadSecure(' ');
            Console.WriteLine();
            Console.Write("domain: ");
            domain = Console.ReadLine();
        }

        private static async Tasks.Task SendEmailAsync(List<string> _servers, int _port, string _from, string _to, string _subject, string _body, bool _protLog)
        {
            Result = 0;
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_from));
            message.To.Add(new MailboxAddress(_to));
            message.Subject = _subject;

            message.Body = new BodyBuilder { TextBody = _body }.ToMessageBody();

            System.IO.Stream stream;
            if (_protLog)
            {
                stream = Console.OpenStandardOutput();
            }
            else
            {
                stream = System.IO.Stream.Null;
            }

            using (var client = new SmtpClient(new ProtocolLogger(stream)))
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.AuthenticationMechanisms.Remove("XOAUTH2"); // Note: only needed if the SMTP server requires authentication client.Authenticate("joey", "password");
                client.MessageSent += (s, e) =>
                {
                    var d = e.Message.Date.UtcDateTime.ToLocalTime();
                    LastSendDate = new DateTime(d.Year,d.Month,d.Day,d.Hour,d.Minute,d.Second,d.Kind);
                    LastSendMessageId = $"<{e.Message.MessageId}>";
                };

                try
                {
                    foreach (var _host in _servers)
                    {
                        try
                        {
                            await Tasks.Task.Run(() =>
                             {
                                client.Connect(_host, _port, false); // Note: since we don't have an OAuth2 token, disable the XOAUTH2 authentication mechanism.
                                client.Send(message);
                             });
                            Result = 0;
                            break;
                        }
                        catch (SocketException e)
                        {
                            Console.WriteLine($"Error connecting smtp server {_host}: {e.Message}");
                            Result = e.GetHashCode();
                        }
                    }
                }
                catch (ServiceNotAuthenticatedException e)
                {
                    Console.WriteLine("WARNING! Authentication Error!");
                    WriteLog("SMTP authentication error!" + Environment.NewLine + e.Message, EventLogEntryType.Error, (int)ErrorCodes.SMTPSendError, 0);
                    Result = e.GetHashCode();
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{e.Message}");
                    WriteLog("SMTP send error!" + Environment.NewLine + e.Message, EventLogEntryType.Error, (int)ErrorCodes.SMTPSendError, 0);
                    Result = e.GetHashCode();
                }
                client.Disconnect(true);
            }
        }
        private static void SubscribeToStreamNotification()
        {
            StreamingSubscription streamingSubscription = exchService.SubscribeToStreamingNotifications(
    new FolderId[] { WellKnownFolderName.Inbox },
    EventType.NewMail);

            StreamingSubscriptionConnection connection = new StreamingSubscriptionConnection(exchService, notifLifetime);

            connection.AddSubscription(streamingSubscription);
            connection.OnNotificationEvent +=
                new StreamingSubscriptionConnection.NotificationEventDelegate(OnEvent);
            connection.OnSubscriptionError +=
                new StreamingSubscriptionConnection.SubscriptionErrorDelegate(OnError);
            connection.OnDisconnect +=
                new StreamingSubscriptionConnection.SubscriptionErrorDelegate(OnDisconnect);
            connection.Open();

            Console.WriteLine("Notification subscription is enabled");
        }

        static void OnEvent(object sender, NotificationEventArgs args)
        {
            StreamingSubscription subscription = args.Subscription;
            // Loop through all item-related events. 
            foreach (NotificationEvent notification in args.Events)
            {
                switch (notification.EventType)
                {
                    case EventType.NewMail:
                        Console.WriteLine("\nE-mail received:");
                        break;
                }
                // The NotificationEvent for an email message is an ItemEvent. 
                // Display the notification identifier. 
                if (notification is ItemEvent itemEvent)
                {
                    try
                    {
                        if (Item.Bind(exchService, itemEvent.ItemId.UniqueId) is EmailMessage mess )
                            if (mess.InternetMessageId == LastSendMessageId)
                            {
                                Console.WriteLine("Message found successfuly!");
                                mess.Delete(DeleteMode.HardDelete);
                                deliveryResult = true;
                            }
                            else
                            {
                                Console.WriteLine($"Message ID does not match: received={mess.InternetMessageId} vs. sent={LastSendMessageId}");
                            }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error comparing message IDs: " + e.Message);
                    }
                }
                else
                {
                    // The NotificationEvent for a folder is a FolderEvent. 
                    FolderEvent folderEvent = (FolderEvent)notification;
                    Console.WriteLine("\nFolderId: " + folderEvent.FolderId.UniqueId);
                }
            }
        }

        static void OnError(object sender, SubscriptionErrorEventArgs args)
        {
            // Handle error conditions. 
            Exception e = args.Exception;
            Console.WriteLine("\nError in notification subscription: " + e.Message);
        }

        static private void OnDisconnect(object sender, SubscriptionErrorEventArgs args)
        {
            // Cast the sender as a StreamingSubscriptionConnection object.           
            StreamingSubscriptionConnection connection = (StreamingSubscriptionConnection)sender;
            // Ask the user if they want to reconnect or close the subscription. 
            Console.WriteLine("The connection to the subscription is disconnected.");
            Console.WriteLine("Reconnecting automatically.");

            connection.Open();
            Console.WriteLine("Connection open.");
            Console.WriteLine("\r\n");
 

        }

        private static async Tasks.Task CheckMailAsync()
        {
            Console.WriteLine("Checking...");

            var searchList = new List<SearchFilter>
            {
                new SearchFilter.ContainsSubstring(EmailMessageSchema.InternetMessageId,LastSendMessageId)
            };
            var searchFilter = new SearchFilter.SearchFilterCollection(LogicalOperator.Or, searchList.ToArray());
            var view = new ItemView(50)
            {
                PropertySet = new PropertySet(BasePropertySet.FirstClassProperties, ItemSchema.Subject, ItemSchema.DateTimeReceived, ItemSchema.DateTimeSent,
                ItemSchema.ConversationId, ItemSchema.Id, EmailMessageSchema.InternetMessageId, ItemSchema.StoreEntryId),
                Traversal = ItemTraversal.Shallow
            };
            view.OrderBy.Add(ItemSchema.DateTimeReceived, SortDirection.Descending);

            FindItemsResults<Item> findResults = null;
            try
            {
                await Tasks.Task.Run(() => findResults = exchService.FindItems(WellKnownFolderName.Inbox, searchFilter, view));
            }
            catch (Exception e)
            {
                Console.WriteLine("Email search error! "+e.Message);
                WriteLog("Email search error!" + Environment.NewLine + e.Message, EventLogEntryType.Error, (int)ErrorCodes.SMTPCheckError, 0);
            }

            if (findResults.Any())
            {
                var mess = findResults.First() as EmailMessage; 
                if (mess?.InternetMessageId == LastSendMessageId)
                {
                    Console.WriteLine("Message found successfuly!");
                    WriteLog("Message found successfuly! Message-ID: "+ mess.InternetMessageId, EventLogEntryType.Information, (int)SuccessCodes.EmailCheckOK, 0);
                    mess.Delete(DeleteMode.HardDelete);
                }

            }
            else
            {
                Console.WriteLine("Message was not found or timed out!");
                WriteLog("Message was not found or timed out! Message-ID: " + LastSendMessageId, EventLogEntryType.Error, (int)ErrorCodes.SMTPCheckError, 0);
            }

        }

        public static List<IPAddress> GetLocalDnsAddresses()
        {
            return NetworkInterface.GetAllNetworkInterfaces()
                .Where(a => a.OperationalStatus == OperationalStatus.Up)
                .Select(a => a.GetIPProperties().DnsAddresses)
                .SelectMany(d => d)
                .Distinct()
                .ToList();
        }

        private static SecureString ReadSecure(char mask='*')
        {
            const int ENTER = 13, BACKSP = 8, CTRLBACKSP = 127;
            int[] FILTERED = { 0, 27, 9, 10 /*, 32 space, if you care */ }; // const
            var securePass = new SecureString();

            char chr = (char)0;

            while ((chr = Console.ReadKey(true).KeyChar) != ENTER)
            {
                if (((chr == BACKSP) || (chr == CTRLBACKSP)) && (securePass.Length > 0))
                {
                    Console.Write("\b \b");
                    securePass.RemoveAt(securePass.Length - 1);
                }
                // Don't append * when length is 0 and backspace is selected
                else if (((chr == BACKSP) || (chr == CTRLBACKSP)) && (securePass.Length == 0))
                {}
                // Don't append when a filtered char is detected
                else if (FILTERED.Contains(chr)) //Count(x => chr == x) > 0
                {}
                // Append and write * mask
                else
                {
                    securePass.AppendChar(chr);
                    Console.Write(mask);
                }
            }
            return securePass;
        }
    }
}
